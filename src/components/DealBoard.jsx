import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
// react-redux
import { useSelector, useDispatch } from "react-redux";
import { getDeals, changeDealStatus } from "../store/features/dealSlice";
import { current } from "@reduxjs/toolkit";

function DealBoard() {
  const dispatch = useDispatch();

  const [currentBoard, setCurrentBoard] = useState();
  const [currentDraggedElement, setCurrentDraggedElement] = useState();

  const [boardId, setBoardId] = useState();
  const [taskId, setTaskId] = useState();

  // const [changableData, setChangableData] = useState([
  //   {
  //     id: 1,
  //     title: "Первый контакт",
  //     items: [
  //       {
  //         id: 1,
  //         manager: "Менеджеров Менеджер",
  //         client: "Клиентов Клиент Клиентович",
  //         date: "01.01.2023",
  //         time: "15:45:10",
  //       },
  //       {
  //         id: 2,
  //         manager: "Менеджеров Менеджер",
  //         client: "Клиентов Клиент Клиентович",
  //         date: "01.01.2023",
  //         time: "15:45:10",
  //       },
  //     ],
  //   },
  //   {
  //     id: 2,
  //     title: "Переговоры",
  //     items: [
  //       {
  //         id: 1,
  //         manager: "Hello world",
  //         client: "Клиентов Клиент Клиентович",
  //         date: "01.01.2023",
  //         time: "15:45:10",
  //       },
  //     ],
  //   },
  //   {
  //     id: 3,
  //     title: "Уже делается сделка",
  //     items: [
  //       {
  //         id: 1,
  //         manager: "Hello world",
  //         client: "Клиентов Клиент Клиентович",
  //         date: "01.01.2023",
  //         time: "15:45:10",
  //       },
  //     ],
  //   },
  // ]);

  useEffect(() => {
    dispatch(getDeals());
  }, []);

  const deals = useSelector((state) => state.deal.deals);
  const isLoading = useSelector((state) => state.deal.isLoading);

  console.log(deals);

  const data = [];

  const dragOver = (e) => {
    e.preventDefault();

    // e.target.style.background = "green"
    // console.log(e.target === 'div' ? 'this div' : 'this is not div');

    // console.log(e.target);
  };

  const dragLeave = (e) => {
    e.preventDefault();
    // console.log(e.target);
    // e.target.background = "green"
  };

  const dragStart = (e, board, draggedElement) => {
    console.log(board.id);
    console.log(draggedElement.client_id);

    setBoardId(board.id);
    setTaskId(draggedElement.client_id);

    setCurrentBoard(board);
    setCurrentDraggedElement(draggedElement);
  };

  const dragEnd = () => { };

  const dropTask = (e, board, draggedElement) => {
    // data.push(...deals.data);
    // if (currentBoard == undefined) return;
    // const currentIndex = currentBoard.items.indexOf(currentDraggedElement);
    // if (currentBoard.title !== board.title && currentIndex != -1) {
    //   data.forEach((item) => {
    //     if (item.title == currentBoard.title) {
    //       item.items.splice(currentIndex, 1);
    //     }
    //     if (item.title === board.title) {
    //       item.items.push(currentDraggedElement);
    //     }
    //   });
    // }
    // setChangableData(data);

    // console.log("draggedBoardId", boardId, "taskId", taskId);
    // console.log("droppedBoardId", board.title);

    if (boardId != board.id) {
      switch (board.title) {
        case "First contact":
          {
            dispatch(changeDealStatus({ id: taskId, type: 1 }));
          }
          break;
        case "Negotiation":
          {
            dispatch(changeDealStatus({ id: taskId, type: 2 }));
          }
          break;
        case "Making a deal":
          {
            dispatch(changeDealStatus({ id: taskId, type: 3 }));
          }
          break;
      }
    }
  };

  const dropBoard = (e, board) => {
    // if (currentBoard == undefined) return;
    // const data = [...changableData];
    // const currentIndex = currentBoard?.items.indexOf(currentDraggedElement);
    // if (
    //   board.items.length == 0 &&
    //   currentBoard.title != board.title &&
    //   currentIndex != -1
    // ) {
    //   data.map((item) => {
    //     if (item.title === currentBoard.title) {
    //       item.items.splice(currentIndex, 1);
    //     }
    //     if (item.title == board.title) {
    //       item.items.push(currentDraggedElement);
    //     }
    //   });
    //   setChangableData(data);
    // }
  };

  return (
    <div className="zadachiData">
      <div className="d-flex">
        {isLoading ? (
          deals &&
          deals.data.map((board, index) => {
            return (
              <div
                key={board.id}
                className="first_deal_phase"
                onDragOver={(e) => dragOver(e)}
                onDrop={(e) => dropBoard(e, board)}
                draggable
              >
                <div
                  className="lidiRed"
                  style={{ background: `${board.class}` }}
                >
                  {board.title}
                </div>
                <div className="d-flex flex-column mt-2">
                  {board.list &&
                    board.list.map((dragElment, index) => {
                      return (
                        <div
                          onClick={() => console.log(board)}
                          key={dragElment.id}
                          className="lidiOfficial lidiMarginRight"
                          onDragOver={(e) => dragOver(e)}
                          onDragLeave={(e) => dragLeave(e)}
                          onDragStart={(e) => dragStart(e, board, dragElment)}
                          onDragEnd={() => dragEnd()}
                          onDrop={(e) => dropTask(e, board, dragElment)}
                          draggable
                        >
                          <p className="zadachiSenderName">
                            Ответственный:{" "}
                            <b>
                              {dragElment.responsible_first_name}{" "}
                              {dragElment.responsible_last_name}{" "}
                              {dragElment.responsible_middle_name}
                            </b>
                          </p>
                          <h3 className="lidiClientov">
                            {dragElment.client_first_name}{" "}
                            {dragElment.client_last_name}{" "}
                            {dragElment.client_middle_name}
                          </h3>
                          <p className="zadachiBlueTime">
                            Дата: {dragElment.day} <br /> Время:
                            {dragElment.time}
                            {/* {dragElment.time} */}
                          </p>
                        </div>
                      );
                    })}
                </div>
              </div>
            );
          })
        ) : (
          <div>Loading...</div>
        )}

        {/* <div className="first_deal_phase">
          <div className="lidiRed">Первый контакт</div>
          <div className="d-flex flex-column mt-2">
            <div className="lidiOfficial lidiMarginRight" draggable>
              <p className="zadachiSenderName">
                Ответственный: <b>Менеджеров Менеджер</b>
              </p>
              <h3 className="lidiClientov">Клиентов Клиент Клиентович</h3>
              <p className="zadachiBlueTime">
                Дата: 01.01.2023 <br /> Время: 15:45:10
              </p>
            </div>
          </div>
        </div> */}

        {/* <div className="second_deal_phase">
          <div className="lidiYellow">Переговоры</div>
          <div className="d-flex flex-column mt-2">
            <div className="lidiOfficial lidiMarginRight">
              <p className="zadachiSenderName">
                Ответственный: <b>Менеджеров Менеджер</b>
              </p>
              <h3 className="lidiClientov">Клиентов Клиент Клиентович</h3>
              <p className="zadachiBlueTime">
                Дата: 01.01.2023 <br /> Время: 15:45:10
              </p>
            </div>
          </div>
        </div> */}

        {/* <div className="third_deal_phase">
          <div className="lidiGreen">Оформление сделки</div>
          <div className="d-flex flex-column mt-2">
            <div className="lidiOfficial lidiMarginRight">
              <p className="zadachiSenderName">
                Ответственный: <b>Менеджеров Менеджер</b>
              </p>
              <h3 className="lidiClientov">Клиентов Клиент Клиентович</h3>
              <p className="zadachiBlueTime">
                Дата: 01.01.2023 <br /> Время: 15:45:10
              </p>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
}

export default DealBoard;
