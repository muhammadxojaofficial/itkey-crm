import React from 'react'
import { NavLink } from 'react-router-dom'
import flagUzb from '../images/1200px-Flag_of_Uzbekistan.png'
import editIcon from '../images/edit.png'
import profileImage from '../images/Ellipse 1.png'
import friendBoyImage3 from '../images/FotoHuman.png'
import eyeIcon from '../images/eye.png'
import britianFlag from '../images/GB.png'
import notificationIcon from '../images/notification.png'
import uzbRegion from '../images/region.png'
import ruRegion from '../images/RU.png'
import deleteIcon from '../images/trash.png'
import bigXImage from '../images/X.png'
// components
import Sidebar from '../components/Sidebar'
import Search from '../components/Search'

function Polzovatel() {
  return (
    <div>
      <div className="d-flex aad">
        <Sidebar />

        <div className="mainMargin">
          <Search />

          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <h2 className="panelUprText">Пользователи</h2>
              <NavLink to={'/newPolzovtel'} className="plus2">
                +
              </NavLink>
            </div>
            <div className="miniSearchDiv7Polzovatel">
              <div className='miniIonIconSearch'>
                <ion-icon
                  className="md hydrated miniSearchIconInput"
                  name="search-outline"
                  role="img"
                  aria-label="search outline"
                />
              </div>
              <input
                placeholder="Поиск по пользователям"
                className="miniInputSdelka6"
                type="text"
              />
            </div>
          </div>

          <div className="polzovatelData">
            <div className="jkMiniData2">
              <div className="checkboxDivInput">
                <input className="checkBoxInput" type="checkbox" />
              </div>
              <div className="checkboxDivInput">№</div>
              <div className="polzovatelFioElectronieAddres">Ф.И.О</div>
              <div className="polzovatelFioElectronieAddres">Электронный адресс</div>
              <div className="pozovatelFoto">Фото</div>
              <div className="checkboxDivTextInput4 polzovatelDeystvieMax">
                Действиe
              </div>
            </div>
            <NavLink to={'/profile'} className="polzovatelMiniData">
              <div className="polzovatelNumber">
                <input className="checkBoxInput" type="checkbox" />
              </div>
              <div className="polzovatelNumber">1</div>
              <div className="polzovatelFioElectronieAddres2">
                Пользователев Пользователь <br /> Пользователивич
              </div>
              <div className="polzovatelFioElectronieAddres2">
                polzovatel,polzovatel@gmail.com
              </div>
              <div className="pozovatelFoto2">
                <img src={friendBoyImage3} alt="HUman" />
              </div>
              <div className="polzovatelEditImg">
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    style={{ marginTop: 4 }}
                    width={25}
                    height={25}
                    src={eyeIcon}
                    alt="Eye"
                  />
                </div>
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    className="mt-1"
                    width={20}
                    height={20}
                    src={editIcon}
                    alt="Edit"
                  />
                </div>
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    className="mt-1"
                    width={20}
                    height={20}
                    src={deleteIcon}
                    alt="Trash"
                  />
                </div>
              </div>
            </NavLink>
            <NavLink to={'/profile'} className="polzovatelMiniData">
              <div className="polzovatelNumber">
                <input className="checkBoxInput" type="checkbox" />
              </div>
              <div className="polzovatelNumber">2</div>
              <div className="polzovatelFioElectronieAddres2">
                Пользователев Пользователь <br /> Пользователивич
              </div>
              <div className="polzovatelFioElectronieAddres2">
                polzovatel,polzovatel@gmail.com
              </div>
              <div className="pozovatelFoto2">
                <img src={bigXImage} alt="X" />
              </div>
              <div className="polzovatelEditImg">
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    style={{ marginTop: 4 }}
                    width={25}
                    height={25}
                    src={eyeIcon}
                    alt="Eye"
                  />
                </div>
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    className="mt-1"
                    width={20}
                    height={20}
                    src={editIcon}
                    alt="Edit"
                  />
                </div>
                <div className="seaDiv" style={{ marginRight: 10 }}>
                  <img
                    className="mt-1"
                    width={20}
                    height={20}
                    src={deleteIcon}
                    alt="Trash"
                  />
                </div>
              </div>
            </NavLink>
          </div>
        </div>
      </div>

    </div>
  )
}

export default Polzovatel