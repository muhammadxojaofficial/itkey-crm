import React from 'react'
import { NavLink } from 'react-router-dom'
import TaskBoard from '../components/TaskBoard'
// components
import Sidebar from '../components/Sidebar'
import Search from '../components/Search'

function Zadachi() {
  return (
    <div>
      <div className="d-flex aad">
        <Sidebar />
        <div className="mainMargin">
          <Search />
          <div className="d-flex justify-content-between">
            <div className="d-flex">
              <h2 className="panelUprText">Задачи</h2>
              <button
                data-toggle="modal"
                data-target="#exampleModal"
                type="button"
                className="plus2"
              >
                +
              </button>
              <NavLink to={'/calendar'} className="kalendarButton">
                Календарь
              </NavLink>
            </div>
          </div>

          <div className="zadachiFlexCenter">
            <div className="d-flex">
              <button className="zadachiFlexVse">
                <input className="zadachiFlexInputCheck" type="checkbox" />
                Все
              </button>
              <button className="zadachiFlexMoiZadachi">
                <input className="zadachiFlexInputCheck" type="checkbox" />
                Мои задачи
              </button>
            </div>
          </div>

          <TaskBoard />
        </div>

        <div
          className="modal fade"
          id="exampleModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content ZadachiModalBody">
              <div className="modal-body">
                <input className="zadachiClient" placeholder="Клиент" type="text" />
                <div className="zadachiBigClientInformation">
                  <div className="textareZadachi" contentEditable="true">
                    <div className="btn-group dropup">
                      <button
                        type="button"
                        className="d-flex chatDropDown dropdown-toggle"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        <div className="smsDlya">
                          <div className="cliendZadachiNaZaftraText">
                            Задача на <span>Завтра</span> для Магомед:{" "}
                            <img src="../images/Call.png" alt="Phone Calling" />
                            Позвонить:
                          </div>
                        </div>
                      </button>
                    </div>
                  </div>
                  <div contentEditable="false" className="d-flex textareaButttonSend">
                    <button className="PostavitButton" data-dismiss="modal">
                      Поставить
                    </button>
                    <button className="OtmenitButton" data-dismiss="modal">
                      Отменить
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Zadachi