import React from 'react'
import { NavLink } from 'react-router-dom'
import flagUzb from '../images/1200px-Flag_of_Uzbekistan.png'
import leftIcon from '../images/arrow-left.png'
import profileImage from '../images/Ellipse 1.png'
import britianFlag from '../images/GB.png'
import notificationIcon from '../images/notification.png'
import uzbRegion from '../images/region.png'
import ruRegion from '../images/RU.png'

// other icons :)
import leftArrowIcon from '../images/icons/arrow-left.png'
// components
import Sidebar from '../components/Sidebar'
import Search from '../components/Search'

function Kalendar() {
    return (
        <>
            <div className="d-flex aad">
                <Sidebar />

                <div className="mainMargin">
                    <Search />

                    <div className="d-flex justify-content-between">
                        <div className="d-flex">
                            <NavLink
                                to={'/lidi'}
                                className="plus2 profileMaxNazadInformatsiyaKlient"
                            >
                                <img src={leftArrowIcon} alt="" />
                            </NavLink>
                            <h2 className="panelUprText">Календарь задач</h2>
                            <button
                                className="plus2 mb-5"
                                data-toggle="modal"
                                data-target="#exampleModal5"
                            >
                                +
                            </button>
                        </div>
                    </div>

                    <div className="zadachiFlexCenter">
                        <div className="d-flex">
                            <button className="zadachiFlexVse">
                                <input className="zadachiFlexInputCheck" type="checkbox" />
                                Все
                            </button>
                            <button className="zadachiFlexMoiZadachi">
                                <input className="zadachiFlexInputCheck" type="checkbox" />
                                Мои задачи
                            </button>
                        </div>
                    </div>

                    <div className="wrapper">
                        <header>
                            <div className="icons calendarCurrent">
                                <span id="prev" className="material-symbols-rounded">
                                    <img src={leftIcon} alt="Left" />
                                </span>
                                <p
                                    className="current-date"
                                    data-toggle="modal"
                                    data-target="#exampleModal4"
                                >
                                    Февраль 2023
                                </p>
                                <span id="next" className="material-symbols-rounded">
                                    <img
                                        className="calendarRightBtn"
                                        src={leftIcon}
                                        alt="Right"
                                    />
                                </span>
                            </div>
                        </header>
                        <div className="calendar">
                            <ul className="weeks">
                                <li className="oneDay">Понедельник</li>
                                <li className="allDay">Вторник</li>
                                <li className="allDay">Среда</li>
                                <li className="allDay">Четверг</li>
                                <li className="allDay">Пятница</li>
                                <li className="allDay">Суббота</li>
                                <li className="endDay">Воскресенье</li>
                            </ul>
                            <ul className="days">
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>1</p>
                                </li>
                                <li
                                    className="daysListRed kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <p>2</p>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>3</p>
                                </li>
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>4</p>
                                </li>
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>5</p>
                                </li>
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>6</p>
                                </li>
                                <li
                                    className="daysList kalendarMrginTopDataRedBlue"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>7</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>8</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>9</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>10</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <p>11</p>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <p>12</p>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>13</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>14</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>15</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>16</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>17</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>18</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <p>19</p>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal"
                                >
                                    <p>20</p>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                    <div className="kalendarImyaRed">Клиентов Клиент Клиентович</div>
                                    <h5>+2 задачи</h5>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>21</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>22</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>23</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>24</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>25</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>26</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>27</p>
                                </li>
                                <li
                                    className="today"
                                    data-toggle="modal"
                                    data-target="#exampleModal2"
                                >
                                    <p>28</p>
                                    <div className="kalendarImyaBlue">Клиентов Клиент Клиентович</div>
                                    <div className="kalendarImyaBlue">Клиентов Клиент Клиентович</div>
                                    <h5>+2 задачи</h5>
                                </li>
                                <li
                                    className="kalendarBorderRadiusLeft daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal2"
                                >
                                    <p>29</p>{" "}
                                    <div className="kalendarImyaBlue">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>30</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>31</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>1</p>
                                </li>
                                <li
                                    className="daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal2"
                                >
                                    <p>2</p>
                                    <div className="kalendarImyaBlue">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="inactive daysList"
                                    data-toggle="modal"
                                    data-target="#exampleModal2"
                                >
                                    <p>3</p>
                                    <div className="kalendarImyaBlue">Клиентов Клиент Клиентович</div>
                                </li>
                                <li
                                    className="inactive daysList kalendarBorderRadiusRight"
                                    data-toggle="modal"
                                    data-target="#exampleModal3"
                                >
                                    <p>4</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div
                className="modal fade"
                id="exampleModal"
                tabIndex={-1}
                role="dialog"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div
                        className="modal-content"
                        style={{
                            borderRadius: "20px !important",
                            border: "2px solid #94B2EB",
                            background: "#F9FBFF"
                        }}
                    >
                        <div className="modal-header d-flex justify-content-center">
                            <div className="modalKalendarDate">28 Декабря 2022</div>
                        </div>
                        <div className="modal-body d-flex justify-content-center flex-column align-items-center">
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                                <div className="modalDataKalendar">
                                    Ответственный: <br /> <b>Менеджеров Менеджеров</b>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBodyBlue">
                                <div className="modalImyaKalendar">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                                <div className="modalDataKalendar">
                                    Ответственный: <br /> <b>Менеджеров Менеджеров</b>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBodyBlue">
                                <div className="modalImyaKalendar">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                                <div className="modalDataKalendar">
                                    Ответственный: <br /> <b>Менеджеров Менеджеров</b>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                                <div className="modalDataKalendar">
                                    Ответственный: <br /> <b>Менеджеров Менеджеров</b>
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                                <div className="modalDataKalendar">
                                    Ответственный: <br /> <b>Менеджеров Менеджеров</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div
                className="modal fade"
                id="exampleModal2"
                tabIndex={-1}
                role="dialog"
                aria-labelledby="exampleModalLabel2"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div
                        className="modal-content"
                        style={{
                            borderRadius: "20px !important",
                            border: "2px solid #94B2EB",
                            background: "#F9FBFF"
                        }}
                    >
                        <div className="modal-header d-flex justify-content-center">
                            <div className="modalKalendarDateBlue">20 Декабря 2022</div>
                        </div>
                        <div className="modal-body d-flex justify-content-center flex-column align-items-center">
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar2">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBodyBlue">
                                <div className="modalImyaKalendar2">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBodyBlue">
                                <div className="modalImyaKalendar2">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar2">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                            </div>
                            <div className="d-flex justify-content-between kalendarModalBody">
                                <div className="modalImyaKalendar2">
                                    Клиентов Клиент Клиентович <br /> 28.12.2022 12:12:12 Встреча
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div
                className="modal fade"
                id="exampleModal3"
                tabIndex={-1}
                role="dialog"
                aria-labelledby="exampleModalLabel3"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div
                        className="modal-content"
                        style={{
                            borderRadius: "20px !important",
                            border: "2px solid #94B2EB",
                            background: "#F9FBFF"
                        }}
                    >
                        <div className="modal-header d-flex justify-content-center">
                            <div className="modalKalendarDateBlue">20 Декабря 2022</div>
                        </div>
                        <div
                            style={{ height: 340 }}
                            className="modal-body d-flex justify-content-center flex-column align-items-center"
                        >
                            <h3 className="modalContentCalendarNet">На сегодня задач нет</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div
                className="modal fade"
                id="exampleModal4"
                tabIndex={-1}
                role="dialog"
                aria-labelledby="exampleModalLabel4"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div
                        className="modal-content"
                        style={{
                            borderRadius: "20px !important",
                            border: "2px solid #94B2EB",
                            background: "#F9FBFF"
                        }}
                    >
                        <div
                            style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20 }}
                            className="modal-body d-flex justify-content-center flex-column align-items-center"
                        >
                            <div className="d-flex">
                                <div className="d-flex flex-column">
                                    <button className="modalMonth">Январь</button>
                                    <button className="modalMonth">Февраль</button>
                                    <button className="modalMonth">Март</button>
                                    <button className="modalMonth">Апрель</button>
                                    <button className="modalMonth">Май</button>
                                    <button className="modalMonth">Июнь</button>
                                </div>
                                <div className="d-flex flex-column">
                                    <button className="modalMonth">Июль</button>
                                    <button className="modalMonth">Август</button>
                                    <button className="modalMonth">Сентябрь</button>
                                    <button className="modalMonth">Октябрь</button>
                                    <button className="modalMonth">Ноябрь</button>
                                    <button className="modalMonth">Декабрь</button>
                                </div>
                                <div className="dropdown">
                                    <button
                                        className="btn modalYearSelect dropdown-toggle"
                                        type="button"
                                        id="dropdownMenuButton"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                    >
                                        2022
                                    </button>
                                    <div
                                        className="dropdown-menu dropdownBodyKalendar"
                                        aria-labelledby="dropdownMenuButton"
                                    >
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2021
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2019
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2018
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2017
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2016
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2015
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2014
                                        </a>
                                        <a className="dropdown-item yearNameKalendar" href="#">
                                            2013
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div
                className="modal fade"
                id="exampleModal5"
                tabIndex={-1}
                role="dialog"
                aria-labelledby="exampleModalLabel5"
                aria-hidden="true"
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content ZadachiModalBody">
                        <div className="modal-body">
                            <input className="zadachiClient" placeholder="Клиент" type="text" />
                            <div className="zadachiBigClientInformation">
                                <div className="textareZadachi" contentEditable="true">
                                    <div className="btn-group dropup">
                                        <button
                                            type="button"
                                            className="d-flex chatDropDown dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                        >
                                            <div className="smsDlya">
                                                <div className="cliendZadachiNaZaftraText">
                                                    Задача на <span>Завтра</span> для Магомед:{" "}
                                                    <img src="../images/Call.png" alt="Phone Calling" />
                                                    Позвонить:
                                                </div>
                                            </div>
                                        </button>
                                    </div>
                                </div>
                                <div contentEditable="false" className="d-flex textareaButttonSend">
                                    <button className="PostavitButton">Поставить</button>
                                    <button className="OtmenitButton">Отменить</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Kalendar